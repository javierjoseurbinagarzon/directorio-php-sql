
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="./src/Favicon.png">
    <title>Empresa | Directorio</title>
    <link rel="stylesheet" type="text/css"
        href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap5.min.css">
    <style>
        *{
            font-family: 'Raleway', sans-serif;
        }
        table.dataTable thead{
            background-color: black;
            color: white;
            
        }
    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

</head>
<header 
style="
width: 100%;
">
    <nav 
    style="
    padding-bottom: 3%;
    padding-top: 3%;
    ">
        <img src="./src/logo.png" 
            style="
            padding-left: 3%;
            ">
    </nav>
    <h3 style="
    text-align: center;
    "><b>Directorio</b></h3>
</header>
<body>
    <div class="container-fluid" style="width: 70%;">
        <div class="row">
            <div class="col">
                <table id="tablaRegistros" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <th>Nombre</th>
                        <th>Cargo</th>
                        <th>Extensión</th>
                        <th>Celular</th>
                    </thead>
                    <tbody>
                        <?php
                        header('Content-Type: text/html; charset=UTF-8');
                        $servername = "ip";
                        $username = "user";
                        $password = "user";
                        $database = "databasename";

                        $connection = new mysqli($servername, $username, $password, $database);
                        $connection -> set_charset("utf8");

                        if ($connection->connect_error){
                            die("connection failed: ". $connection->connect_error);
                        }

                        $sql = "SELECT * FROM Directorio";
                        $result = $connection->query($sql);

                        if (!$result) {
                            die("Invalid Query: " . $connection->error);
                        }

                        while($row = $result->fetch_assoc()) {
                            echo "<tr>
                            <td>" . $row["Nombre"] . "</td>
                            <td>" . $row["Cargo"] . "</td>
                            <td>" . $row["Extension"] . "</td>
                            <td>" . $row["Celular"] . "</td>
                        </td>";
                        }
                        ?>
                    </tbody>
                </table>
                <p style="padding-top: 20px; text-align: center;">
                - Para realizar una llamada Local, marcar:  9 + 60 + 1 + Número de teléfono <br>
                - Para realizar una llamada Nacional, marcar:  9 + 60 + código de area + Número de teléfono <br>
                - Para realizar una llamada Internacional, marcar:  9 + 007 + Indicativo + Código de área + Número de teléfono <br>
                - Para realizar una llamada a Celular, marcar:  9 + Número de Celular
                </p>
            </div>
        </div>
    </div>





    <!-- tables -->
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" language="javascript"
        src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript"
        src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#tablaRegistros').DataTable(
                {
            "pageLength": 100
            }
            );
        });
    </script>
</body>

</html>